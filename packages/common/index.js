"use strict";

import COLOURS from "./design/colours";
import SPACING from "./design/spacing";
import TYPOGRAPHY from "./design/typography";

module.exports = {
  COLOURS,
  SPACING,
  TYPOGRAPHY,
};
