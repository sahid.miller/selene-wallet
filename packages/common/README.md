# `@selene-wallet/common`

Shared functions and utilities for other BCH libraries.

Note: This repo is part of the Selene monorepo. Refer to the central documentation for more required information.

[NPM Packages](https://www.npmjs.com/org/selene-wallet)

[Gitlab source](https://gitlab.com/selene.cash/selene-wallet)

## Dev

After any changes, need to compile to Typescript

```
$ rm -rf ./dist && npx tsc --noEmit false --outDir ./dist
```
