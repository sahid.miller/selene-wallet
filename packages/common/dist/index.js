"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const colours_1 = __importDefault(require("./design/colours"));
const spacing_1 = __importDefault(require("./design/spacing"));
const typography_1 = __importDefault(require("./design/typography"));
module.exports = {
    COLOURS: colours_1.default,
    SPACING: spacing_1.default,
    TYPOGRAPHY: typography_1.default,
};
//# sourceMappingURL=index.js.map