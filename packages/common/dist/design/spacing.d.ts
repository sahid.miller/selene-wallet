declare const SPACING: {
    five: number;
    ten: number;
    fifteen: number;
    twentyFive: number;
    fifty: number;
    borderRadius: number;
};
export default SPACING;
