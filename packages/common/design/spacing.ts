const SPACING = {
  five: 5,
  ten: 10,
  fifteen: 15,
  twentyFive: 25,
  fifty: 50,
  borderRadius: 10,
};

export default SPACING;
