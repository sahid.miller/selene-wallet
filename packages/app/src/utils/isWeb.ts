import { Platform } from "react-native";

// Platform wide utility
export const IS_WEB = Platform.OS === "web";
