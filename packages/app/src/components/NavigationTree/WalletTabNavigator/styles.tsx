import COLOURS from "@selene-wallet/common/design/colours";
import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  tabBar: {
    backgroundColor: COLOURS.white,
    borderTopWidth: 0,
  },
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    backgroundColor: COLOURS.black,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
};

export default styles;
