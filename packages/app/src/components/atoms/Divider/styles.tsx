import SPACING from "@selene-wallet/common/design/spacing";
import COLOURS from "@selene-wallet/common/design/colours";

const styles = {
  divider: {
    margin: SPACING.five,
    paddingBottom: SPACING.ten,
    borderTopColor: COLOURS.lightGrey,
    borderTopWidth: 1,
    width: "100%",
  },
};

export default styles;
