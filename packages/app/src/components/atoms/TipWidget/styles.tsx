import SPACING from "@selene-wallet/common/design/spacing";

const styles = {
  container: {
    paddingTop: SPACING.five,
    marginVertical: SPACING.five,
    height: 80,
    minHeight: 80,
  },
  wrapper: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    height: 65,
    minHeight: 65,
  },
};

export default styles;
